package com.binus;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int chooseMenu;

        ToDoList todo = new ToDoList();

        do{
            Display menu = new Display();
            menu.initMainMenu();

            chooseMenu = scanner.nextInt();
            scanner.nextLine();

            if(chooseMenu ==1){
                todo.addToDoList();
            }
            else if(chooseMenu == 2){
                todo.updateToDoList();
            }
            else if(chooseMenu == 3){
                todo.removeToDoList();
            }
            else if(chooseMenu == 4){
                todo.selectToDoListByCategory();
            }
            else if(chooseMenu == 5){
                todo.selectToDoListByTaskID();
            }
            else if(chooseMenu == 6){
                todo.viewToDoList();
            }
        }
        while(chooseMenu != 7);
    }
}
