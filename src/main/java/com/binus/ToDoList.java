package com.binus;

import java.util.*;

public class ToDoList {
    private static Scanner scanner = new Scanner(System.in);
    private ArrayList<Task> toDoList;

    public void addToDo(int taskID, String taskName, String taskCategory){
        if(toDoList==null){
            this.toDoList = new ArrayList<>();
        }
        Task task = new Task(taskID, taskName, taskCategory);
        toDoList.add(task);
    }

    public String getToDoByIndex(int index){
        Task selected = toDoList.get(index);

        return selected.toString();
    }

    String getToDoList() {
        StringBuilder todoList = new StringBuilder();

        for (int i = 0; i < toDoList.size(); i++) {
            todoList.append(getToDoByIndex(i));
            if (i < toDoList.size() - 1) {
                todoList.append("\n");
            }
        }
        return todoList.toString();
    }

    void addToDoList(){
        int taskID;
        String taskName;
        String taskCategory;

        System.out.println("Input Task ID: ");
        taskID = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Input Task Name: ");
        taskName = scanner.nextLine();

        System.out.println("Input Task Category: ");
        taskCategory = scanner.nextLine();

        addToDo(taskID, taskName, taskCategory);
    }

    void updateToDoList(){
        int index;

        System.out.println("Choose Task ID: ");
        index = scanner.nextInt();
        System.out.println(updateToDoStatus(index));
        System.out.println();
    }

    String updateToDoStatus(int index) {
        for(Task task: toDoList){
            if (task.getTaskID() == index){
                if(task.isStatus()) {
                    return "Task Already Updated";
                }
                task.setTaskStatus();
                return "Task Status Updated";
            }
        }
        return "Task Not Found";
    }

    void removeToDoList(){
        int index;

        System.out.println("Choose Task ID: ");
        index= scanner.nextInt();
        System.out.println(removeToDoListByIndex(index));
        System.out.println();
    }

    String removeToDoListByIndex(int index) {
        for (Task task : toDoList) {
            if (task.getTaskID() == index) {
                toDoList.remove(task);
                return "Task Removed";
            }
        }
        return "Task Not Found";
    }

    void selectToDoListByCategory(){
        String categoryName;

        System.out.println(showAllCategory().toString());
        System.out.println("Choose Task Category: ");
        categoryName = scanner.nextLine();
        System.out.println(getTaskByCategory(categoryName));
        System.out.println();
    }

    private ArrayList showAllCategory() {
        ArrayList<String> toDoCategory = new ArrayList<>();

        for (Task task : toDoList) {
            if (!toDoCategory.contains(task.getTaskCategory())) {
                toDoCategory.add(task.getTaskCategory());
            }
        }
        return toDoCategory;
    }

    String getTaskByCategory(String category) {
        StringBuilder todoList = new StringBuilder();

        for (Task task : toDoList)
            if (task.isCategory(category)) {
                todoList.append(task.toString()).append("\n");
            }
        todoList.delete(todoList.length() - 1, todoList.length());
        return todoList.toString();
    }

    void selectToDoListByTaskID(){
        int index;

        System.out.println("Choose Task ID: ");
        index= scanner.nextInt();
        System.out.println(getTaskByID(index));
        System.out.println();
    }

    String getTaskByID(int id) {
        for(Task task: toDoList){
            if(task.getTaskID() == id){
                return task.toString();
            }
        }
        return "Task Not Found";
    }

    void viewToDoList(){
        System.out.println(getToDoList());
        System.out.println();
    }
}
