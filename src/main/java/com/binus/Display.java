package com.binus;

class Display {
    void initMainMenu(){
        System.out.println("--TO DO LIST--");
        System.out.println("1. Add To Do List");
        System.out.println("2. Update To Do List");
        System.out.println("3. Remove To Do List");
        System.out.println("4. Search Task by Category");
        System.out.println("5. Search Task by ID");
        System.out.println("6. Display All To Do List");
        System.out.println("7. Exit");
        System.out.println("Choose: ");
    }
}
