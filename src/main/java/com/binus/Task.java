package com.binus;

import java.util.Objects;

public class Task {
    private int taskID;
    private String taskName;
    private String taskStatus;
    private String taskCategory;

    Task(int taskID, String taskName, String taskCategory){
        this.taskID = taskID;
        this.taskName = taskName;
        this.taskStatus = "NOT DONE";
        this.taskCategory = taskCategory;
    }

    void setTaskStatus(){
        this.taskStatus = "DONE";
    }

    int getTaskID() {
        return taskID;
    }

    String getTaskStatus(){
        return this.taskStatus;
    }

    String getTaskCategory() {
        return this.taskCategory;
    }

    Boolean isCategory(String category){
        return getTaskCategory().equalsIgnoreCase(category);
    }

    Boolean isStatus(){
        return getTaskStatus().equals("DONE");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return taskID == task.taskID &&
                Objects.equals(taskName, task.taskName) &&
                Objects.equals(taskStatus, task.taskStatus) &&
                Objects.equals(taskCategory, task.taskCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskID, taskName, taskStatus, taskCategory);
    }

    @Override
    public String toString() {
        return this.taskID + ". " + this.taskName + " " + this.taskCategory + " " + "[" + this.taskStatus + "]";
    }
}
