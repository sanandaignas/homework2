package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {
    @Test
    public void testAddToDoByIndex(){
        String expectedTask = "1. Belajar Urgent [NOT DONE]";

        ToDoList myToDoList = new ToDoList();
        myToDoList.addToDo(1,"Belajar", "Urgent");

        assertEquals(expectedTask, myToDoList.getToDoByIndex(0));
    }

    @Test
    public void testAddToDoList(){
        String expectedTask = "1. Ngoding Urgent [NOT DONE]\n2. Mandi Urgent [NOT DONE]\n3. Makan Urgent [NOT DONE]";

        ToDoList toDoListMany = new ToDoList();
        toDoListMany.addToDo(1,"Ngoding", "Urgent");
        toDoListMany.addToDo(2, "Mandi", "Urgent");
        toDoListMany.addToDo(3, "Makan", "Urgent");

        assertEquals(expectedTask, toDoListMany.getToDoList());
    }

    @Test
    public void testUpdateTask(){
        String expected = "1. Ngoding Urgent [DONE]";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Ngoding", "Urgent");
        todoList.updateToDoStatus(1);

        assertEquals(expected, todoList.getToDoList());
    }

    @Test
    public void testUpdateTaskNotFound(){
        String expected = "Task Not Found";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Ngoding", "Urgent");

        assertEquals(expected, todoList.updateToDoStatus(0));
    }

    @Test
    public void testRemoveTaskByID(){
        String expectedTodo = "1. Belajar School [NOT DONE]\n2. Ngoding School [NOT DONE]";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Belajar", "School");
        todoList.addToDo(2, "Ngoding", "School");
        todoList.addToDo(3, "Mandi", "Urgent");
        todoList.removeToDoListByIndex(3);

        String actualTodo = todoList.getToDoList();

        assertEquals(expectedTodo, actualTodo);
    }

    @Test
    public void testRemoveTaskNotFound(){
        String expected = "Task Not Found";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Ngoding", "Urgent");

        assertEquals(expected, todoList.updateToDoStatus(0));
    }

    @Test
    public void testShowByCategory() {
        String expectedToDo = "3. Mandi Urgent [NOT DONE]";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Belajar", "School");
        todoList.addToDo(2, "Ngoding", "School");
        todoList.addToDo(3, "Mandi", "Urgent");

        assertEquals(expectedToDo,todoList.getTaskByCategory("Urgent"));
    }

    @Test
    public void testShowByID() {
        String expectedToDo = "3. Mandi Urgent [NOT DONE]";

        ToDoList todoList = new ToDoList();
        todoList.addToDo(1, "Belajar", "School");
        todoList.addToDo(2, "Ngoding", "School");
        todoList.addToDo(3, "Mandi", "Urgent");

        assertEquals(expectedToDo,todoList.getTaskByID(3));
    }
}